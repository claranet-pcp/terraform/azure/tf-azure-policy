resource "azurerm_policy_definition" "audit_classic_resource_create" {
  name         = "audit-classic-resource-create"
  policy_type  = "Custom"
  mode         = "All"
  display_name = "Policy to generate an audit event upon creation of classic/v1 (i.e., ASM-based) resources"

  policy_rule = <<POLICY_RULE
    {
  "if": {
    "anyOf": [
      {
        "field": "type",
        "like": "Microsoft.ClassicCompute/*"
      },
      {
        "field": "type",
        "like": "microsoft.classicStorage/*"
      },
      {
        "field": "type",
        "like": "Microsoft.ClassicNetwork/*"
      },
      {
        "field": "type",
        "like": "Microsoft.Backup/*"
      }
    ]
  },
  "then": {
    "effect": "audit"
  }
}
POLICY_RULE
}

resource "azurerm_policy_definition" "audit_job_scheduler_free_tier" {
  name         = "audit-job-scheduler-free-tier"
  policy_type  = "Custom"
  mode         = "All"
  display_name = "Policy to generate an audit event upon creation of a job scheduler with free tier"

  policy_rule = <<POLICY_RULE
    {
  "if": {
    "allOf": [
      {
        "field": "type",
        "equals": "Microsoft.Scheduler/jobcollections"
      },
      {
        "field": "Microsoft.Scheduler/jobcollections/sku.name",
        "equals": "Free"
      }
    ]
  },
  "then": {
    "effect": "audit"
  }
}
POLICY_RULE
}

resource "azurerm_policy_definition" "audit_nongrs_storage_sku" {
  name         = "audit-nongrs-storage-sku"
  policy_type  = "Custom"
  mode         = "All"
  display_name = "Policy to generate an audit event upon creation of a storage account with Standard LRS (no Geo-Replication)"

  policy_rule = <<POLICY_RULE
    {
  "if": {
    "allOf": [
      {
        "field": "type",
        "equals": "Microsoft.Storage/storageAccounts"
      },
      {
        "field": "Microsoft.Storage/storageAccounts/sku.name",
        "equals": "Standard_LRS"
      }
    ]
  },
  "then": {
    "effect": "audit"
  }
}
POLICY_RULE
}

resource "azurerm_policy_definition" "audit_nonhbi_resource_create" {
  name         = "audit-nonhbi-resource-create"
  policy_type  = "Custom"
  mode         = "All"
  display_name = "Policy to generate an audit event upon creation of resource types which have not been ratified for critical enterprise data"

  policy_rule = <<POLICY_RULE
    {
  "if": {
    "not": {
      "anyOf": [
        {
          "field": "type",
          "like": "Microsoft.DataFactory/*"
        },
        {
          "field": "type",
          "like": "Microsoft.DataLakeAnalytics/*"
        },
        {
          "field": "type",
          "like": "Microsoft.DataLakeStore/*"
        },
        {
          "field": "type",
          "like": "Microsoft.Web/*"
        },
        {
          "field": "type",
          "like": "Microsoft.Search/*"
        },
        {
          "field": "type",
          "like": "Microsoft.ServiceBus/*"
        },
        {
          "field": "type",
          "like": "Microsoft.Relay/*"
        },
        {
          "field": "type",
          "like": "Microsoft.EventHub/*"
        },
        {
          "field": "type",
          "like": "Microsoft.Logic/*"
        },
        {
          "field": "type",
          "like": "Microsoft.NotificationHubs/*"
        },
        {
          "field": "type",
          "like": "Microsoft.ServiceFabric/*"
        },
        {
          "field": "type",
          "like": "Microsoft.Sql/*"
        },
        {
          "field": "type",
          "like": "Microsoft.Compute/*"
        },
        {
          "field": "type",
          "like": "Microsoft.Storage/*"
        },
        {
          "field": "type",
          "like": "Microsoft.AnalysisServices/*"
        },
        {
          "field": "type",
          "like": "Microsoft.Automation/*"
        },
        {
          "field": "type",
          "like": "Microsoft.Cdn/*"
        },
        {
          "field": "type",
          "like": "Microsoft.DocumentDb/*"
        },
        {
          "field": "type",
          "like": "Microsoft.Network/*"
        },
        {
          "field": "type",
          "like": "Microsoft.KeyVault/*"
        },
        {
          "field": "type",
          "like": "Microsoft.Cache/*"
        },
        {
          "field": "type",
          "like": "Microsoft.StreamAnalytics/*"
        },
        {
          "field": "type",
          "like": "Microsoft.Compute/*"
        }
      ]
    }
  },
  "then": {
    "effect": "audit"
  }
}
POLICY_RULE
}

resource "azurerm_policy_definition" "audit_old_sql_version" {
  name         = "audit-old-sql-version"
  policy_type  = "Custom"
  mode         = "All"
  display_name = "Policy to generate an audit event upon creation of a sql server with version less than 12.0"

  policy_rule = <<POLICY_RULE
    {
  "if": {
    "allOf": [
      {
        "field": "type",
        "equals": "Microsoft.SQL/servers"
      },
      {
        "field": "Microsoft.SQL/servers/version",
        "in": [
          "2.0",
          "3.0",
          "4.0",
          "5.0",
          "6.0",
          "7.0",
          "8.0",
          "9.0",
          "10.0",
          "11.0"
        ]
      }
    ]
  },
  "then": {
    "effect": "audit"
  }
}
POLICY_RULE
}

resource "azurerm_policy_definition" "audit_sql_basic_create" {
  name         = "audit-sql-basic-create"
  policy_type  = "Custom"
  mode         = "All"
  display_name = "Policy to generate an audit event upon creation of a SQL database with 'Basic' edition type"

  policy_rule = <<POLICY_RULE
    {
  "if": {
    "field": "Microsoft.SQL/servers/databases/edition",
    "equals": "Basic"
  },
  "then": {
    "effect": "audit"
  }
}
POLICY_RULE
}

resource "azurerm_policy_definition" "deny_classic_resource_create" {
  name         = "deny-classic-resource-create"
  policy_type  = "Custom"
  mode         = "All"
  display_name = "Policy to deny upon creation of classic/v1 (i.e., ASM-based) resources"

  policy_rule = <<POLICY_RULE
{
  "if": {
    "anyOf": [
      {
        "field": "type",
        "like": "Microsoft.ClassicCompute/*"
      },
      {
        "field": "type",
        "like": "microsoft.classicStorage/*"
      },
      {
        "field": "type",
        "like": "Microsoft.ClassicNetwork/*"
      }
    ]
  },
  "then": {
    "effect": "deny"
  }
}
POLICY_RULE
}

resource "azurerm_policy_definition" "tags_enforcement_policy" {
  name         = "tags-enforcement-policy"
  policy_type  = "Custom"
  mode         = "All"
  display_name = "Policy to enforce the correct tags on resources"

  policy_rule = <<POLICY_RULE
{
  "if": {
    "not": {
      "anyOf": [
        {
          "field": "tags",
          "exists": "true"
        },
        {
          "field": "tags.ServiceNowIncident",
          "exists": "true"
        },
        {
          "field": "tags.Approver",
          "exists": "true"
        },
        {
          "field": "tags.Approver",
          "like": "*@arup.com"
        },
        {
          "field": "tags.Environment",
          "exists": "true"
        },
        {
          "field": "tags.Environment",
          "in": [
            "prod",
            "dev/test"
          ]
        },
        {
          "field": "tags.CostCenter",
          "exists": "true"
        },
        {
          "field": "tags.JobNumber",
          "exists": "true"
        },
        {
          "field": "tags.DataClassification",
          "exists": "true"
        },
        {
          "field": "tags.DataClassification",
          "in": [
            "Confidential",
            "Internal",
            "Public",
            "Restricted"
          ]
        },
        {
          "field": "tags.SecurityZone",
          "exists": "true"
        },
        {
          "field": "tags.SecurityZone",
          "in": [
            "Private",
            "Public"
          ]
        }
      ]
    }
  },
  "then": {
    "effect": "audit"
  }
}
POLICY_RULE
}

resource "azurerm_policy_definition" "stop_g_series_vms" {
  name         = "stop-g-series-vms"
  policy_type  = "Custom"
  mode         = "All"
  display_name = "Stop G Series VMs"

  policy_rule = <<POLICY_RULE
{
  "if": {
    "allOf": [
      {
        "field": "type",
        "equals": "Microsoft.Compute/virtualMachines"
      },
      {
        "field": "Microsoft.Compute/virtualMachines/sku.name",
        "like": "Standard_G*"
      }
    ]
  },
  "then": {
    "effect": "deny"
  }
}
POLICY_RULE
}

resource "azurerm_policy_definition" "stop_m_series_vms" {
  name         = "stop-m-series-vms"
  policy_type  = "Custom"
  mode         = "All"
  display_name = "Stop M Series VMs"

  policy_rule = <<POLICY_RULE
{
  "if": {
    "allOf": [
      {
        "field": "type",
        "equals": "Microsoft.Compute/virtualMachines"
      },
      {
        "field": "Microsoft.Compute/virtualMachines/sku.name",
        "like": "Standard_M*"
      }
    ]
  },
  "then": {
    "effect": "deny"
  }
}
POLICY_RULE
}
