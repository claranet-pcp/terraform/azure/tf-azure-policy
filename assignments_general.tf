resource "azurerm_policy_assignment" "enforce_tags" {
  count = "${length(var.requiredtags) * var.enforce_tags_enabled}"

  name                 = "enforce-tag ${count.index}"
  scope                = "${var.scope}"
  policy_definition_id = "${local.policy_ids["enforce_tag"]}"
  description          = "Enforces a required tag and its value. Does not apply to resource groups."
  display_name         = "Enforce ${element(keys(var.requiredtags), count.index)} tag and its default value"

  parameters = <<PARAMETERS
{
    "tagName": {
        "value":  "${element(keys(var.requiredtags), count.index)}"
    },
    "tagValue": {
        "value": "${lookup(var.requiredtags, (element(keys(var.requiredtags), count.index)))}"
    }
}
PARAMETERS
}

#TODO: enforce tags on resource groups

#TODO: only supports one location at the moment
resource "azurerm_policy_assignment" "allow_locations" {
  count = "${var.allow_locations_enabled}"

  name                 = "allowed-locations"
  scope                = "${var.scope}"
  policy_definition_id = "${local.policy_ids["allowed_locations"]}"
  description          = "This policy enables you to restrict the locations your organization can specify when deploying resources"
  display_name         = "Restrict the locations that can be deployed to"

  parameters = <<PARAMETERS
{
  "listOfAllowedLocations": {
    "value": ["${element(var.locations,0)}"]
  }
}
PARAMETERS
}

#TODO: allowed locations on resource groups


#TODO: replace audit RBAC with enforce
# resource "azurerm_policy_assignment" "audit_rbac" {
#   count = "${var.audit_rbac_enabled}"
#   name                 = "audit-rbac"
#   scope                = "${azurerm_resource_group.test.id}"
#   policy_definition_id = "/providers/Microsoft.Authorization/policyDefinitions/a451c1ef-c6ca-483d-87ed-f49761e3ffb5"
#   description          = "Audit built-in roles such as 'Owner, Contributer, Reader' instead of custom RBAC roles, which are error prone"
#   display_name         = "audit-rbac"
# }

