variable scope {}

variable "requiredtags" {
  type    = "map"
  default = {}
}

variable "locations" {
  type    = "list"
  default = ["UK West", "UK South"]
}

variable "storage_skus" {
  type    = "list"
  default = ["ZRS", "GRS"]
}

variable "compute_skus" {
  type    = "list"
  default = ["Basic_A0", "Basic_A1"]
}

locals {
  policy_prefix = "/providers/Microsoft.Authorization/policyDefinitions"

  policy_ids = {
    enforce_tag                     = "${local.policy_prefix}/1e30110a-5ceb-460c-a204-c1c3969c6d62"
    allowed_locations               = "${local.policy_prefix}/e56962a6-4747-49cd-b67b-bf8b01975c4c"
    allowed_storage_skus            = "${local.policy_prefix}/7433c107-6db4-4ad1-b57a-a76dce0154a1"
    audit_storage_secure_transfer   = "${local.policy_prefix}/404c3081-a854-4457-ae30-26a93ef643f9"
    audit_storage_network_access    = "${local.policy_prefix}/34c877ad-507e-4c82-993e-3452a6e0ad3c"
    audit_storage_classic_accounts  = "${local.policy_prefix}/37e0d2fe-28a5-43d6-a273-67d37d1f5606"
    require_storage_blob_encryption = "${local.policy_prefix}/7c5a74bf-ae94-4a74-8fcf-644d1e0e6e6f"
    allow_vm_skus                   = "${local.policy_prefix}/cccc23c7-8427-4f53-ad12-b6a63eb452b3"
    audit_diagnostic_logs_vmss      = "${local.policy_prefix}/7c1b1214-f927-48bf-8882-84f0af6588b1"
    audit_classic_vms               = "${local.policy_prefix}/1d84d5fb-01f6-4d12-ba4f-4a26081d403d"
    audit_vms_without_managed_disks = "${local.policy_prefix}/06a78e20-9358-41c9-923c-fb736d382a4d"
  }
}

variable "allow_vm_skus_enabled" {
  default = false
}

variable "audit_diagnostic_logs_vmss_enabled" {
  default = false
}

variable "audit_classic_vms_enabled" {
  default = false
}

variable "audit_vms_without_managed_disks_enabled" {
  default = false
}

variable "enforce_tags_enabled" {
  default = false
}

variable "allow_locations_enabled" {
  default = false
}

variable "audit_rbac_enabled" {
  default = false
}

variable "allowed_storage_skus_enabled" {
  default = false
}

variable "audit_storage_secure_transfer_enabled" {
  default = false
}

variable "audit_storage_network_access_enabled" {
  default = false
}

variable "audit_storage_classic_accounts_enabled" {
  default = false
}

variable "require_storage_blob_encryption_enabled" {
  default = false
}

## custom resources
variable "audit_classic_resource_create_enabled" {
  default = false
}

variable "audit_job_scheduler_free_tier_enabled" {
  default = false
}

variable "audit_nongrs_storage_sku_enabled" {
  default = false
}

variable "audit_nonhbi_resource_create_enabled" {
  default = false
}

variable "audit_old_sql_version_enabled" {
  default = false
}

variable "audit_sql_basic_create_enabled" {
  default = false
}

variable "deny_classic_resource_create_enabled" {
  default = false
}

variable "tags_enforcement_policy_enabled" {
  default = false
}

variable "stop_g_series_vms_enabled" {
  default = false
}

variable "stop_m_series_vms_enabled" {
  default = false
}
