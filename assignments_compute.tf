resource "azurerm_policy_assignment" "allow_vm_skus" {
  count = "${var.allow_vm_skus_enabled}"

  name                 = "allow-vm-skus"
  scope                = "${var.scope}"
  policy_definition_id = "${local.policy_ids["allow_vm_skus"]}"
  description          = "This policy enables you to specify a set of virtual machine SKUs that your organization can deploy"
  display_name         = "Allow Virtual Machine SKUs"

  parameters = <<PARAMETERS
{
  "listOfAllowedSKUs": {
    "value": [${join(",",formatlist("\"%s\"", var.compute_skus))}]
  }
}
PARAMETERS
}

# TODO: implement custom enforce
resource "azurerm_policy_assignment" "audit_diagnostic_logs_vmss" {
  count = "${var.audit_diagnostic_logs_vmss_enabled}"

  name                 = "audit-diagnostic-logs-vmss"
  scope                = "${var.scope}"
  policy_definition_id = "${local.policy_ids["audit_diagnostic_logs_vmss"]}"
  description          = "Audit enabling of diagnostics logs in Service Fabric and Virtual Machine Scale Sets"
  display_name         = "Audit Dianostic Logs for VMSS and Service Fabric"
}

# Only audit, not enforcing because there is no way in ARM to create classic accounts
resource "azurerm_policy_assignment" "audit_classic_vms" {
  count = "${var.audit_classic_vms_enabled}"

  name                 = "audit-classic-vms"
  scope                = "${var.scope}"
  policy_definition_id = "${local.policy_ids["audit_classic_vms"]}"
  description          = "Audit use of classic virtual machines"
  display_name         = "Audit use of classic virtual machines"
}

/*data "template_file" "unmanaged_disks_policy" {
  template = "${file("${path.module}/templates/unmanaged.tpl")}"
}

resource "azurerm_policy_definition" "deny_unmanaged_disks" {
  name  = "deny-unmanaged-disks"
  policy_type = "Custom"
  mode = "All"
  display_name = "Deny Unmanaged Disks"

  policy_rule = "${data.template_file.unmanaged_disks_policy.rendered}"
}

resource "azurerm_policy_assignment" "deny_unmanaged_disks" {
  name  = "deny-unmanaged-disks"
  scope  = "${var.scope}"
  policy_definition_id = "${azurerm_policy_definition.deny_unmanaged_disks.id}"
  description = ""
  display_name = ""
}*/

# TODO: implement custom enforce
resource "azurerm_policy_assignment" "audit_vms_without_managed_disks" {
  count = "${var.audit_vms_without_managed_disks_enabled}"

  name                 = "audit-vms-without-managed-disks"
  scope                = "${var.scope}"
  policy_definition_id = "${local.policy_ids["audit_vms_without_managed_disks"]}"
  description          = "Audit VMs that do not use managed disks"
  display_name         = "Audit VMs that do not use managed disks"
}
