# Example

```
module "azure_policy" {
  source = "../modules/tf-azure-policy"
  scope  = "/subscriptions/ed5ece22-89e1-486d-ba83-8035ab420121"

  requiredtags = {
    Owner      = "Stephen.Hendry@arup.com"
    CostCenter = "01-648"
    JobNumber  = "077997-41"
  }
}
```

Enforce = prevent on new resource, non-compliant on old

Apply = auto-create on new
