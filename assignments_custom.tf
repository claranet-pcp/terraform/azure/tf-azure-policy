resource "azurerm_policy_assignment" "audit_classic_resource_create" {
  count = "${var.audit_classic_resource_create_enabled}"

  name                 = "audit-classic-resource-create"
  scope                = "${var.scope}"
  policy_definition_id = "${azurerm_policy_definition.audit_classic_resource_create.id}"
  description          = "Audit to generate an event upon creation of classic/v1 (i.e., ASM-based) resources"
  display_name         = "Audit creation of classic/v1 (i.e., ASM-based) resources"
}

resource "azurerm_policy_assignment" "audit_job_scheduler_free_tier" {
  count = "${var.audit_job_scheduler_free_tier_enabled}"

  name                 = "audit-job-scheduler-free_tier"
  scope                = "${var.scope}"
  policy_definition_id = "${azurerm_policy_definition.audit_job_scheduler_free_tier.id}"
  description          = "Audit to generate an event upon creation of a job scheduler with free tier"
  display_name         = "Audit creation of a job scheduler with free tier"
}

resource "azurerm_policy_assignment" "audit_nongrs_storage_sku" {
  count = "${var.audit_nongrs_storage_sku_enabled}"

  name                 = "audit-nongrs-storage-sku"
  scope                = "${var.scope}"
  policy_definition_id = "${azurerm_policy_definition.audit_nongrs_storage_sku.id}"
  description          = "Audit to generate an event upon creation of a storage account with Standard LRS (no Geo-Replication)"
  display_name         = "Audit creation of a storage account with Standard LRS (no Geo-Replication)"
}

resource "azurerm_policy_assignment" "audit_nonhbi_resource_create" {
  count = "${var.audit_nonhbi_resource_create_enabled}"

  name                 = "audit-nonhbi-resource-create"
  scope                = "${var.scope}"
  policy_definition_id = "${azurerm_policy_definition.audit_nonhbi_resource_create.id}"
  description          = "Audit to generate an event upon creation of resource types which have not been ratified for critical enterprise data"
  display_name         = "Audit creation of a resource types which have not been ratified for critical enterprise data"
}

resource "azurerm_policy_assignment" "audit_old_sql_version" {
  count = "${var.audit_old_sql_version_enabled}"

  name                 = "audit-old-sql-version"
  scope                = "${var.scope}"
  policy_definition_id = "${azurerm_policy_definition.audit_old_sql_version.id}"
  description          = "Audit to generate an event upon creation of a sql server with version less than 12.0"
  display_name         = "Audit creation of a sql server with version less than 12.0"
}

resource "azurerm_policy_assignment" "audit_sql_basic_create" {
  count = "${var.audit_sql_basic_create_enabled}"

  name                 = "audit-sql-basic-create"
  scope                = "${var.scope}"
  policy_definition_id = "${azurerm_policy_definition.audit_sql_basic_create.id}"
  description          = "Audit to generate an event upon upon creation of a SQL database with 'Basic' edition type"
  display_name         = "Audit creation of a SQL database with 'Basic' edition type"
}

resource "azurerm_policy_assignment" "deny_classic_resource_create" {
  count = "${var.deny_classic_resource_create_enabled}"

  name                 = "deny-classic-resource-create"
  scope                = "${var.scope}"
  policy_definition_id = "${azurerm_policy_definition.deny_classic_resource_create.id}"
  description          = "Policy to deny upon creation of classic/v1 (i.e., ASM-based) resources"
  display_name         = "Deny creation of classic/v1 (i.e., ASM-based) resources"
}

resource "azurerm_policy_assignment" "tags_enforcement_policy" {
  count = "${var.tags_enforcement_policy_enabled}"

  name                 = "tags-enforcement-policy"
  scope                = "${var.scope}"
  policy_definition_id = "${azurerm_policy_definition.tags_enforcement_policy.id}"
  description          = "Policy to enforce the correct tags on resources"
  display_name         = "Enforce the correct tags on resources"
}

resource "azurerm_policy_assignment" "stop_g_series_vms" {
  count = "${var.stop_g_series_vms_enabled}"

  name                 = "stop-g-series-vms"
  scope                = "${var.scope}"
  policy_definition_id = "${azurerm_policy_definition.stop_g_series_vms.id}"
  description          = "Stop G Series VMs"
  display_name         = "Stop G Series VMs"
}

resource "azurerm_policy_assignment" "stop_m_series_vms" {
  count = "${var.stop_m_series_vms_enabled}"

  name                 = "stop-m-series-vms"
  scope                = "${var.scope}"
  policy_definition_id = "${azurerm_policy_definition.stop_m_series_vms.id}"
  description          = "Stop M Series VMs"
  display_name         = "Stop M Series VMs"
}
