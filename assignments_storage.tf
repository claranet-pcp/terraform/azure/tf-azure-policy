resource "azurerm_policy_assignment" "allowed_storage_skus" {
  count = "${var.allowed_storage_skus_enabled}"

  name                 = "allowed-storage-skus"
  scope                = "${var.scope}"
  policy_definition_id = "${local.policy_ids["allowed_storage_skus"]}"
  description          = "This policy enables you to specify a set of storage account SKUs that your organization can deploy"
  display_name         = "Allow Storage SKUs"

  parameters = <<PARAMETERS
{
  "listOfAllowedSKUs": {
    "value": [${join(",",formatlist("\"%s\"", var.storage_skus))}]
  }
}
PARAMETERS
}

# TODO: implement custom enforce
resource "azurerm_policy_assignment" "audit_storage_secure_transfer" {
  count = "${var.audit_storage_secure_transfer_enabled}"

  name                 = "aduit-storage-secure-transfer"
  scope                = "${var.scope}"
  policy_definition_id = "${local.policy_ids["audit_storage_secure_transfer"]}"
  description          = "Audit requirment of Secure transfer in your storage account"
  display_name         = "Audit Storage Secure Transfer"
}

# TODO: implement custom enforce
resource "azurerm_policy_assignment" "audit_storage_network_access" {
  count = "${var.audit_storage_network_access_enabled}"

  name                 = "aduit-storage-network_access"
  scope                = "${var.scope}"
  policy_definition_id = "${local.policy_ids["audit_storage_network_access"]}"
  description          = "Audit unrestricted network access in your storage account firewall settings"
  display_name         = "Audit Storage Network Acesss"
}

# TODO: implement custom enforce
resource "azurerm_policy_assignment" "audit_storage_classic_accounts" {
  count = "${var.audit_storage_classic_accounts_enabled}"

  name                 = "aduit-storage-classic_accounts"
  scope                = "${var.scope}"
  policy_definition_id = "${local.policy_ids["audit_storage_classic_accounts"]}"
  description          = "Audit use of classic storage accounts"
  display_name         = "Audit Storage Classic Accounts"
}

resource "azurerm_policy_assignment" "require_storage_blob_encryption" {
  count = "${var.require_storage_blob_encryption_enabled}"

  name                 = "require-storage-blob-encryption"
  scope                = "${var.scope}"
  policy_definition_id = "${local.policy_ids["require_storage_blob_encryption"]}"
  description          = "This policy ensures blob encryption for storage accounts is turned on"
  display_name         = "Require Storage Blob Encryption"
}
